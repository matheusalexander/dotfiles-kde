function Colors(color)
	color = color or "kanagawa"
	vim.cmd.colorscheme(color)
	vim.api.nvim_set_hl(0, "Normal", { bg = "none" })
	vim.api.nvim_set_hl(0, "NormalFloat", { bg = "none" })
  vim.api.nvim_set_hl(0, "LineNrAbove", { bg = "none", fg="#727169" })
  vim.api.nvim_set_hl(0, "LineNr", { bg = "none", fg="#938AA9", bold=true })
  vim.api.nvim_set_hl(0, "LineNrBelow", { bg = "none", fg="#727169" })
  -- vim.cmd.highlight("clear SignColumn")
  vim.api.nvim_set_hl(0, "SignColumn", { bg = "none" })
end

Colors()
