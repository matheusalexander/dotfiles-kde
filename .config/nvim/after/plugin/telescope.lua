local builtin = require('telescope.builtin')
-- List project files
vim.keymap.set('n', '<leader>pf', builtin.find_files, {})
-- List git files
vim.keymap.set('n', '<leader>gf', builtin.git_files, {})
-- Look for a string within the project
vim.keymap.set('n', '<leader>ps', builtin.live_grep, {})
-- List buffers
vim.keymap.set('n', '<leader>bb', builtin.buffers, {})
