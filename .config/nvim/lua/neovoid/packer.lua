--
-- PACKER: The plugin manager
--
vim.cmd [[packadd packer.nvim]]

return require('packer').startup(function(use)
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'

--
-- TELESCOPE: Fuzzy finder
--

-- Dependencies
  use "nvim-lua/plenary.nvim"

-- Telescope per se
  use {
    'nvim-telescope/telescope.nvim', tag = '0.1.2',
  -- or                            , branch = '0.1.x',
    requires = { {'nvim-lua/plenary.nvim'} }
  }

--
-- KANAGAWA: Colour scheme 
--

  use({
    'rebelot/kanagawa.nvim',
    as = 'kanagawa',
    config = function()
	    vim.cmd('colorscheme kanagawa')
    end
  })

--
-- TREESITTER: Parse generator
--
  use('nvim-treesitter/nvim-treesitter', {run = ':TSUpdate'})


--
-- UNDOTREE: File changes manager
--
  use('mbbill/undotree')

--
-- FUGITIVE: Git integration
--
  use('tpope/vim-fugitive')

--
-- LSP: Language Server Protocol plugins
--
  use {
    'VonHeikemen/lsp-zero.nvim',
    branch = 'v2.x',
    requires = {
      -- LSP Support
      {'neovim/nvim-lspconfig'},             -- Required
      {'williamboman/mason.nvim'},           -- Optional
      {'williamboman/mason-lspconfig.nvim'}, -- Optional

      -- Autocompletion
      {'hrsh7th/nvim-cmp'},     -- Required
      {'hrsh7th/cmp-nvim-lsp'}, -- Required
      {'L3MON4D3/LuaSnip'},     -- Required
    }
  }

end)
