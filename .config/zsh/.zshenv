# Path
export PATH="$PATH:$HOME/.local/share/scripts:$HOME/.local/bin"

# Defaults
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox"
export READER="zathura"
export FILE="lf"
export VIEWER="feh"

# Organizing files and locations
export LESSHISTFILE="$HOME/.cache/lesshist"
export PASSWORD_STORE_DIR="$HOME/.local/share/password-store"
