# Prompt
autoload -U colors && colors
PROMPT="%F{magenta}%n%f at %F{green}%m%f - %# "
RPROMPT="[%~] %F{cyan}%T%f"

# History
HISTFILE=~/.cache/zhistory
HISTSIZE=100
SAVEHIST=5000

# CD without Cd
setopt autocd

# No beep
unsetopt beep

# Vi mode
bindkey -v

# Completion
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)

# Aliases and Shortcuts
[ -f "$HOME/.config/zsh/aliases" ] && source "$HOME/.config/zsh/aliases"
[ -f "$HOME/.config/zsh/shortcuts" ] && source "$HOME/.config/zsh/shortcuts"

# Highlight plugin
source "$HOME/.config/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
